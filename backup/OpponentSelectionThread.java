package com.example.tictactoebt;

import java.util.ArrayList;

import android.bluetooth.BluetoothDevice;
import android.os.AsyncTask;

public class OpponentSelectionThread extends AsyncTask<ArrayList<BluetoothDevice>, Void, String>
{

    @Override
    protected String doInBackground(ArrayList<BluetoothDevice>... params)
    {
	int size = params[0].size();

	int selection = (int)(Math.random()*size);
	// TODO Auto-generated method stub
	return ((BluetoothDevice)params[0].toArray()[selection]).getAddress();
    }
    
    protected void onPostExecute(String selection)
    {
	HomeActivity.setOpponent(selection);
    }

}
