package com.example.tictactoebt;

public class Board
{
    private char[][] board;
    private int numMoves;
    private boolean isInit;

    public Board()
    {
	board = new char[3][3];
	isInit = false;

    }
    
    /*
     *  arbitrary initialize to allow for proper checking... it's threaded anyway
     *  so will not affect UI
     */

    public void init() 
    {

	char[] temp1 =
	{ 'a', 'b', 'c' };
	char[] temp2 =
	{ 'd', 'e', 'f' };
	char[] temp3 =
	{ 'g', 'h', 'i' };

	board[0] = temp1;
	board[1] = temp2;
	board[2] = temp3;
	isInit = true;
    }
    
    public boolean isInit()
    {
	return isInit;
    }

    public void inputMove(int loc, char playerSym) //add a move to the Board
    {
	board[loc / 3][loc % 3] = playerSym; 
	numMoves++;
    }

    public String checkWin()
    {
	if (numMoves > 5)
	{
	    for (int row = 0; row < 3; row++) // check each row for a winner, if
					      // exists return
	    {
		if (board[row][0] == board[row][1] //check rows
			&& board[row][0] == board[row][2])
		    return Character.toString(board[row][0]);

		for (int col = 0; col < 3; col++) //check cols
		{
		    if (board[0][col] == board[1][col]
			    && board[0][col] == board[2][col])
			return Character.toString(board[row][0]);

		}
		if (board[0][0] == board[1][1] && board[0][0] == board[2][2]) //check diag
		{
		    return Character.toString(board[0][0]);
		}
		if (board[0][2] == board[1][1] && board[0][2] == board[2][0]) //check anti-diag
		{
		    return Character.toString(board[2][0]);
		}
	    }
	}
	return null;
    }

    public char[][] getBoard()
    {
	return board;
    }
}
