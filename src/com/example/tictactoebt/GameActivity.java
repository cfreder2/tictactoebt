package com.example.tictactoebt;

import android.app.Activity;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.ImageButton;

public class GameActivity extends Activity
{
    private Board board;
    private char playerSym;
    private char winner;
    private ImageButton[] boardButtons;
    
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.game);
	
	board = new Board();
	
	
	//initialize buttons for manipulation
	boardButtons[0] = (ImageButton) findViewById(R.id.pos0);
	boardButtons[1] = (ImageButton) findViewById(R.id.pos1);
	boardButtons[2] = (ImageButton) findViewById(R.id.pos2);
	boardButtons[3] = (ImageButton) findViewById(R.id.pos3);
	boardButtons[4] = (ImageButton) findViewById(R.id.pos4);
	boardButtons[5] = (ImageButton) findViewById(R.id.pos5);
	boardButtons[6] = (ImageButton) findViewById(R.id.pos6);
	boardButtons[7] = (ImageButton) findViewById(R.id.pos7);
	boardButtons[8] = (ImageButton) findViewById(R.id.pos8);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.game, menu);
	return true;
    }
    
    /*
     * each of the 9 ImageButtons correspond to a method below, which execute
     * a GameEngineThread with corresponding parameters to manipulate the game board
     * 
     */
    
    public void inputMove0(View view) //called when a button is pressed 
    {
	new GameEngineThread().execute(new Integer(0), new Character('x'));
    }
    public void inputMove1(View view) //called when a button is pressed 
    {
	new GameEngineThread().execute(new Integer(1), new Character('x'));
    }
    public void inputMove2(View view) //called when a button is pressed 
    {
	new GameEngineThread().execute(new Integer(2), new Character('x'));
    }
    public void inputMove3(View view) //called when a button is pressed 
    {
	new GameEngineThread().execute(new Integer(3), new Character('x'));
    }
    public void inputMove4(View view) //called when a button is pressed 
    {
	new GameEngineThread().execute(new Integer(4), new Character('x'));
    }
    public void inputMove5(View view) //called when a button is pressed 
    {
	new GameEngineThread().execute(new Integer(5), new Character('x'));
    }
    public void inputMove6(View view) //called when a button is pressed 
    {
	new GameEngineThread().execute(new Integer(6), new Character('x'));
    }
    public void inputMove7(View view) //called when a button is pressed 
    {
	new GameEngineThread().execute(new Integer(7), new Character('x'));
    }
    public void inputMove8(View view) //called when a button is pressed 
    {
	new GameEngineThread().execute(new Integer(8), new Character('x'));
    }
    
    private class GameEngineThread extends AsyncTask<Object, Void, Boolean> //gameboard updater thread
    {
        /*
         * first parameter: position on board to place your symbol
         * 0 1 2
         * 3 4 5
         * 6 7 8
         * 
         */
        @Override
        protected Boolean doInBackground(Object... params)
        {
            int loc = ((Integer)params[0]).intValue();
            char playerSym = ((Character)params[1]).charValue();
            String checkedWinner;
            
            if (!board.isInit()) //check if board is initialized, if not --> initialize
            {
        	board.init();
            }
            board.inputMove(loc,playerSym); //input move
            checkedWinner = board.checkWin(); //check if is winning move
            if (checkedWinner != null)
            {
        	winner = checkedWinner.charAt(0);
        	if (winner == playerSym)
        	{
        	    //you won!
        	}
        	else
        	{
        	    //you lost.
        	}
        	//end bluetooth comm
        	
            }
    	
    	    // TODO Auto-generated method stub
            return null;
        }

    }

}
