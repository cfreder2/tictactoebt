package com.example.tictactoebt;

import java.util.ArrayList;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class HomeActivity extends Activity
{
    private static final String PKG_PREFIX = "com.example.tictactoebt.OPPONENT";
    private Button allowChallengesButton;
    private Button sendChallenge;
    private TextView waiting;
    private BluetoothAdapter myAdapter;
    // private ArrayList<BluetoothDevice> pairedDevices;
    private ArrayList<BluetoothDevice> opponents;
    private boolean initialBT;
    private boolean isActive;
    private boolean isWaiting;
    private BroadcastReceiver opponentReceiver;
    private String opponent;
    private TextView stats;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.main);

	myAdapter = BluetoothAdapter.getDefaultAdapter();
	allowChallengesButton = (Button) findViewById(R.id.allow_challenges);
	sendChallenge = (Button) findViewById(R.id.send_challenge);
	waiting = (TextView) findViewById(R.id.waiting);
	stats = (TextView) findViewById(R.id.stats);
	opponents = new ArrayList<BluetoothDevice>();
	sendChallenge.setClickable(false);
	isActive = false;
	isWaiting = false;
	new StatCalculator().execute();

	// initialBT = myAdapter.isEnabled();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
	// Inflate the menu; this adds items to the action bar if it is present.
	getMenuInflater().inflate(R.menu.main, menu);
	return true;
    }

    public void controlBT(View view)
    {
	if (!isActive)
	{
	    new BTControl().execute();
	}

	else
	{
	    /*
	     * if (!initialBT) //not working properly but atleast it turns off
	     * bluetooth { myAdapter.disable(); }
	     */
	    myAdapter.disable(); // ignores initial bluetooth setting and turns
				 // on bluetooth
	    allowChallengesButton.setText("Allow challenges"); // reset button
							       // label
	    sendChallenge.setClickable(false);

	}

	isActive = !isActive;
	sendChallenge.setClickable(isActive);
    }

    @SuppressWarnings("unchecked")
    public void sendChallenge() // send out a challenge
    {
	allowChallengesButton.setClickable(false);
	sendChallenge.setText("Cancel challenge");
	sendChallenge.setOnClickListener(new View.OnClickListener()
	{
	    public void onClick(View v)
	    {
		cancelChallenge();
	    }
	});
	waiting.setText("Searching for opponent...");

	/*
	 * 
	 * would be used to find a specific device, but in this case I will use
	 * a random device selected in a concurrent thread from device discovery
	 * 
	 * 
	 * Set<BluetoothDevice> pairedDevices = myAdapter.getBondedDevices(); //
	 * If there are paired devices if (pairedDevices.size() > 0) { // Loop
	 * through paired devices for (BluetoothDevice device : pairedDevices) {
	 * // Add the name and address to an array adapter to show in a ListView
	 * pairedAddrs.add(device.getAddress()); } }
	 */

	final BroadcastReceiver opponentReceiver = new BroadcastReceiver()
	{
	    public void onReceive(Context context, Intent intent)
	    {
		String action = intent.getAction();
		// When discovery finds a device
		if (BluetoothDevice.ACTION_FOUND.equals(action))
		{
		    // Get the BluetoothDevice object from the Intent
		    BluetoothDevice device = intent
			    .getParcelableExtra(BluetoothDevice.EXTRA_DEVICE);
		    // Add the name and address to an array adapter to show in a
		    // ListView
		    opponents.add(device);
		}
	    }
	};

	if (opponents.size() > 0)
	{
	    new OpponentSelectionThread().execute(opponents);
	}
	// Register the BroadcastReceiver
	IntentFilter filter = new IntentFilter(BluetoothDevice.ACTION_FOUND);
	registerReceiver(opponentReceiver, filter); // Don't forget to
						    // unregister during
						    // onDestroy

    }

    public void cancelChallenge()
    {
	myAdapter.cancelDiscovery();
	sendChallenge.setOnClickListener(new View.OnClickListener()
	{
	    public void onClick(View v)
	    {
		sendChallenge();
	    }
	});
	waiting.setText("");
	sendChallenge.setText("Send challenge");
    }

    public void opponentFound()
    {
	
	Intent intent = new Intent(this, GameActivity.class);
	intent.putExtra(PKG_PREFIX, opponent);
	startActivity(intent);
    }

    public void setOpponent(String b)
    {
	opponent = b;

    }

    protected void onDestroy()
    {
	unregisterReceiver(opponentReceiver);
	Log.i("destroy", "receiver unregistered");
    }
    
    private class BTControl extends AsyncTask<Void, Void, String>
    {

	@Override
	protected String doInBackground(Void... params)
	{
		if (!isActive)
		{
		    Intent discoverableIntent = new Intent(BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
		    discoverableIntent.putExtra(BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 30);
		    startActivity(discoverableIntent);
		}
		return null;
	}
		
	protected void onPostExecute(String str)
	{
	    allowChallengesButton.setText("Disallow challenges");
	    sendChallenge.setOnClickListener(new View.OnClickListener()
	    {
		public void onClick(View v)
		{
		    sendChallenge();
		}
	    });
	}
	}
	
    

    
    /*
     * thread to concurrently choose a random opponent out of list of discovered opponenents
     */
    private class OpponentSelectionThread extends  
	    AsyncTask<ArrayList<BluetoothDevice>, Void, String>
    {

	@Override
	protected String doInBackground(ArrayList<BluetoothDevice>... params)
	{
	    int size = params[0].size();

	    int selection = (int) (Math.random() * size);
	    // TODO Auto-generated method stub
	    return ((BluetoothDevice) params[0].toArray()[selection]).getAddress();
	}

	protected void onPostExecute(String selection)
	{
	    opponent = selection;
	    opponentFound();

	}

    }
    
    private class StatCalculator extends AsyncTask<Object, Void, Object>
    {

        @Override
        protected Object doInBackground(Object... params)
        {
            // evalute statistics
            // 
            
            
    	// TODO Auto-generated method stub
    	return null;
        }
        
        protected void onPostExecute(Object obj)
        {
            stats.setText("");
        }
    }
}
